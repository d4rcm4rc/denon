/*
    Copyright (C) 2016-2020  Marc André Wittorf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ASIO_HELPERS_INC
#define ASIO_HELPERS_INC

#include <list>
#include <memory>
#include <tuple>

#include <denon/compat.hpp>

#include <boost/asio.hpp>
#include <boost/optional.hpp>

namespace AsioHelpers{
	template<typename T>
	class AsioQueue {
	public:
		AsioQueue(denon_compat::boost_io_context &io)  : io(io) {}
		
		void push(const T &elem) {
			if(this->cb) {
				std::function<void(T&&)> cb=this->cb;
				this->cb=nullptr;
				cb(std::move(elem));
			}
			else {
				this->queue.push_back(std::move(elem));
			}
		}
		
		void push(T &&elem) {
			if(this->cb) {
				std::function<void(T&&)> cb=this->cb;
				this->cb=nullptr;
				cb(std::move(elem));
			}
			else {
				this->queue.push_back(std::move(elem));
			}
		}
		
		template <typename... Args>
		void emplace(Args&&... args) {
			if(this->cb) {
				std::function<void(T&&)> cb=this->cb;
				this->cb=nullptr;
				cb(T(std::forward<Args>(args)...));
			}
			else {
				this->queue.emplace_back(std::forward<Args>(args)...);
			}
		}
		
		template<typename CompletionToken>
		auto pop(CompletionToken &&token) {
			if(cb)
				throw new std::logic_error("Invalid state");
#if BOOST_VERSION < 106600
			typename boost::asio::handler_type<CompletionToken, void(T)>::type handler(std::forward<CompletionToken>(token));
			boost::asio::async_result<decltype(handler)> result(handler);
#else
			CompletionToken handler(std::forward<CompletionToken>(token));
			boost::asio::async_result<CompletionToken, void(T)> result(handler);
#endif
			
			std::function<void(T&&)> cb = [&io = this->io, handler=std::move(handler)](T &&t){
				denon_compat::boost_post(io, boost::bind<void>(handler, t));
			};
			
			if(queue.empty())
				this->cb=cb;
			else {
				T elem(std::move(this->queue.front()));
				this->queue.pop_front();
				cb(std::move(elem));
			}
			
			return result.get();
		}
		
	private:
		denon_compat::boost_io_context &io;
		std::function<void(T&&)> cb = nullptr;
		std::list<T> queue;
	};

	template<typename T>
	class AsioMultiConsumerQueue : public std::enable_shared_from_this<AsioMultiConsumerQueue<T>> {
	private:
		typedef AsioMultiConsumerQueue<T> InstantiatedSelfType;
		
		struct ListItem {
			boost::optional<T> elem;
			size_t numReadBy = 0;
			unsigned int diffNumConsumers = 0;
			//ListItem(size_t numRemainingConsumers) : numRemainingConsumers(numRemainingConsumers) {};
		};
		//typedef std::tuple<boost::optional<T>,size_t> ListItem; //TODO O(n) when adding or deleting an Endpoint to O(1)
		typedef std::list<ListItem> List;
		typedef typename List::iterator ListIterator;
		
	public:
		
		class Endpoint : public std::enable_shared_from_this<Endpoint> {
			friend class AsioMultiConsumerQueue<T>;
		
		public:
			template<typename CompletionToken>
			auto pop(CompletionToken &&token) {
				if(waiting)
					throw new std::logic_error("Invalid state");
#if BOOST_VERSION < 106600
				typename boost::asio::handler_type<CompletionToken, void(T)>::type handler(std::forward<CompletionToken>(token));
				boost::asio::async_result<decltype(handler)> result(handler);
#else
				CompletionToken handler(std::forward<CompletionToken>(token));
				boost::asio::async_result<CompletionToken, void(T)> result(handler);
#endif
				std::function<void(const T&, bool)> cb = [self = this->shared_from_this(), handler=std::move(handler)](const T &t, bool advanceIterator){
					if(advanceIterator)
						++self->nextElement;
					self->waiting=false;
					self->owner->io.post(boost::bind<void>(handler, t));
				};
				nextElement->numReadBy++;
				if(nextElement->elem == boost::none) {
					waiting = true;
					owner->waiters.push_back(cb);
				} else {
					cb(*(nextElement->elem),true);
				}
				owner->collectGarbage();
				return result.get();
			}
			
			~Endpoint() {
				--(nextElement->diffNumConsumers);
				owner->collectGarbage();
			}
			
		private:
			ListIterator nextElement;
			std::shared_ptr<InstantiatedSelfType> owner;
			bool waiting=false;
			
			Endpoint(std::shared_ptr<InstantiatedSelfType> &&owner, ListIterator &&it) : owner(owner), nextElement(std::move(it)) {
				++(nextElement->diffNumConsumers);
			}
		};
		
		static std::shared_ptr<InstantiatedSelfType> make(denon_compat::boost_io_context &io) {
			return std::shared_ptr<InstantiatedSelfType>(new InstantiatedSelfType(io));
		}
		
		AsioMultiConsumerQueue(const AsioMultiConsumerQueue&) = delete;
		AsioMultiConsumerQueue& operator=(const AsioMultiConsumerQueue&) = delete;
		
		void push(const T &elem) {
			//If all consumers are already waiting, we can just pass them the element
			//no need to allocate space in the queue
			bool advanceIterator=false;
			if(waiters.size() != numConsumersPush + queue.back().diffNumConsumers || flagKeepBacklog) {
				numConsumersPush+=queue.back().diffNumConsumers;
				queue.back().elem=elem;
				queue.back().numReadBy=waiters.size();
				queue.emplace_back();
				advanceIterator=true;
			}
			decltype(waiters) myWaiters;
			myWaiters.swap(waiters);
			for(auto w : myWaiters)
				w(elem,advanceIterator);
		}
		
		std::shared_ptr<Endpoint> newEndpoint(bool backlog = false) {
			ListIterator it;
			if(backlog)
				it = queue.begin();
			else {
				it = queue.end();
				--it;
			}
			return std::shared_ptr<Endpoint>(new Endpoint(this->shared_from_this(), std::move(it)));
		}
		
		void keepBacklog(bool keep) {flagKeepBacklog=keep; if(!keep) collectGarbage();}

	private:
		denon_compat::boost_io_context &io;

		size_t numConsumersGC;
		size_t numConsumersPush;
		List queue;
		std::list<std::function<void(const T&, bool)>> waiters;
		bool flagKeepBacklog=false;
		
		AsioMultiConsumerQueue(denon_compat::boost_io_context &io) : io(io), queue(1){}
		
		void collectGarbage(void) {
			if(flagKeepBacklog)
				return;
			while(queue.front().elem != boost::none && queue.front().numReadBy == numConsumersGC + queue.front().diffNumConsumers) {
				numConsumersGC += queue.front().diffNumConsumers;
				queue.pop_front();
			}
		}
	};
}

#endif //ASIO_HELPERS_INC
