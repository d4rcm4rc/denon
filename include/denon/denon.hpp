/*
    Copyright (C) 2016-2020  Marc André Wittorf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DENON_HPP_INC
#define DENON_HPP_INC

#include <memory>
#include <regex>

#include <denon/compat.hpp>
#define BOOST_BIND_NO_PLACEHOLDERS
#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <denon/asio_helpers.hpp>

namespace Denon {
	namespace Impl {
		template<typename T>
		struct suspender_if_exists
		{
			// SFINAE foo-has-correct-sig :)
			template<typename A>
			static std::true_type test(void (A::*)()) {
				return std::true_type();
			}
			
			// SFINAE suspend-exists :)
			template <typename A>
			static decltype(test(&A::suspend))
			test(decltype(&A::suspend),void *) {
				// suspend exists. What about sig?
				typedef decltype(test(&A::suspend)) return_type;
				return return_type();
			}
			
			// SFINAE game over :(
			template<typename A>
			static std::false_type test(...) {
				return std::false_type();
			}
			
			// This will be either `std::true_type` or `std::false_type`
			typedef decltype(test<T>(0,0)) type;
			
			static const bool value = type::value; // Which is it?
			
			static void eval(T& t, std::true_type) {
				t.suspend();
			}
			
			// `eval(...)` is a no-op for otherwise unmatched arguments
			static void eval(T& t, std::false_type){}
			
			static void eval(T& t) {
				eval(t,type());
			}
		};
	}

	template<typename Connection=std::unique_ptr<boost::asio::ip::tcp::socket>, int BufferSize=135>
	class DenonConnection : public std::enable_shared_from_this<DenonConnection<Connection,BufferSize>> {
	private:
		typedef DenonConnection<Connection,BufferSize> InstantiatedSelfType;
	public:
		typedef std::shared_ptr<InstantiatedSelfType> ptr;
		
		static std::shared_ptr<InstantiatedSelfType> make(Connection &&conn) {
			return std::shared_ptr<InstantiatedSelfType>(new InstantiatedSelfType(std::move(conn)));
		}
		
		DenonConnection(const DenonConnection&) = delete;
		DenonConnection& operator=(const DenonConnection&) = delete;
		
		template<typename CompletionToken>
		auto nextCommand(CompletionToken &&token) {
#if BOOST_VERSION < 106600
			typename boost::asio::handler_type<CompletionToken, void(std::string command)>::type handler(std::forward<CompletionToken>(token));
			boost::asio::async_result<decltype(handler)> result(handler);
#else
			CompletionToken handler(std::forward<CompletionToken>(token));
			boost::asio::async_result<CompletionToken, void(std::string command)> result(handler);
#endif
			boost::asio::async_read_until(
				*conn, readBuffer, '\r',
				[self = this->shared_from_this(), handler = std::move(handler)](const boost::system::error_code& ec, std::size_t size) {
					if(!ec) {
						std::string command(size-1,0);
						std::istream stream(&self->readBuffer);
						stream.read(&command[0], size-1);
						self->readBuffer.consume(1);
						if(std::regex_match(command,self->validCommands))
							denon_compat::boost_dispatch(
								denon_compat::boost_get_executor(*self->conn),
								boost::bind<void>(handler, ec, command)
							);
						else
							denon_compat::boost_dispatch(
								denon_compat::boost_get_executor(*self->conn),
								boost::bind<void>(handler, boost::asio::error::not_found, command)
							);
					} else if (ec == boost::asio::error::not_found) {
						self->clearInput(handler);
					} else {
						denon_compat::boost_dispatch(
							denon_compat::boost_get_executor(*self->conn),
							boost::bind<void>(handler, ec, std::string())
						);
					}
				}
			);
			return result.get();
		}
		
#if 0 //TODO: add synchronous implementation?
		std::size_t sendCommand(const std::string &command) {
			std::string modCommand(command);
			modCommand.append(1,'\r');
			return boost::asio::write(*conn, boost::asio::buffer(modCommand));
		}
#endif
		
		template<typename CompletionToken>
		auto sendCommand(const std::string &command, CompletionToken &&token) {
			auto size = std::min(writeBuffer.size()-1,command.size());
			std::copy_n(command.begin(),size,writeBuffer.begin());
			writeBuffer[size]='\r';
			return boost::asio::async_write(*conn, boost::asio::buffer(writeBuffer,size+1), token);
		}
		
		void setValidCommandsMatcher(const std::regex &validCommands) {
			this->validCommands=validCommands;
		}
		
		const std::regex& getValidCommands() {
			return validCommands;
		}
		
		void close() {
			Impl::suspender_if_exists<typename Connection::element_type>::eval(*conn);
			conn->close();
		}

	private:
		Connection conn;
		boost::asio::streambuf readBuffer;
		std::array<char, BufferSize> writeBuffer;
		DenonConnection(Connection &&conn) : conn(std::move(conn)), readBuffer(BufferSize), validCommands(".*") {}
		std::regex validCommands;
		
		template<typename Handler>
		void clearInput(Handler handler) {
			readBuffer.consume(readBuffer.size());
			boost::asio::async_read_until(
				*conn, readBuffer, '\r',
				[self = this->shared_from_this(), handler = std::move(handler)](const boost::system::error_code& ec, std::size_t size) {
					if(!ec) {
						self->readBuffer.consume(size);
						denon_compat::boost_dispatch(
							denon_compat::boost_get_executor(*self->conn),
							boost::bind<void>(handler, boost::asio::error::not_found, std::string())
						);
					} else if (ec == boost::asio::error::not_found) {
						self->clearInput(handler);
					} else {
						denon_compat::boost_dispatch(
							denon_compat::boost_get_executor(*self->conn),
							boost::bind<void>(handler, ec, std::string())
						);
					}
				}
			);
		}
	};

	typedef DenonConnection<std::unique_ptr<boost::asio::serial_port>> SerialDenon;
	typedef DenonConnection<std::shared_ptr<boost::asio::ip::tcp::socket>> TCPDenon;
	typedef DenonConnection<std::shared_ptr<boost::asio::local::stream_protocol::socket>> UnixDenon;

	static std::unique_ptr<boost::asio::serial_port> MakeDenonSerialConnection(
		denon_compat::boost_io_context &io,
		const std::string &device
	) {
		auto ser(std::make_unique<boost::asio::serial_port>(io, device));
		boost::asio::serial_port_base::baud_rate BAUD(9600);
		boost::asio::serial_port_base::character_size CHARSIZE( 8 );
		boost::asio::serial_port_base::flow_control FLOW( boost::asio::serial_port_base::flow_control::none );
		boost::asio::serial_port_base::parity PARITY( boost::asio::serial_port_base::parity::none );
		boost::asio::serial_port_base::stop_bits STOP( boost::asio::serial_port_base::stop_bits::one );
		
		ser->set_option( BAUD );
		ser->set_option( CHARSIZE );
		ser->set_option( FLOW );
		ser->set_option( PARITY );
		ser->set_option( STOP );
		
		return ser;
	}
}

#endif //DENON_HPP_INC
