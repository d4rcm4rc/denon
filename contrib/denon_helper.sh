#!/bin/bash

## CONFIGURABLES

DENON_POWER_CHANNEL=5
SWITCHBOX_INTERFACE_SOCK="/var/run/switchbox_interface.sock"
DENON="denon -u /var/run/denond.sock"

POWER_COMMUNICATE="nc -U $SWITCHBOX_INTERFACE_SOCK"

function send_power_command() {
	echo -n "$1$DENON_POWER_CHANNEL" | $POWER_COMMUNICATE
}

function check_response() {
	if echo "$1" | grep "$2" > /dev/null; then
		return 0
	else
		return 1
	fi
}

function denon_turn_power_status() {
	local res=$(send_power_command "$1")
	if check_response "$res" "^k$"; then
		return 0
	else
		return 1
	fi
}

function denon_ensure_power_status_real() {
	local res=$(send_power_command g)
	if check_response "$res" "$2"; then
		return 0
	elif check_response "$res" "$3"; then
		if denon_turn_power_status "$1"; then
			local res=$(send_power_command g)
			if check_response "$res" "$2"; then
				return 0
			fi
		fi
	fi
	return 1
}

function denon_ensure_power_status() {
	if denon_ensure_power_status_real "$1" "$2" "$3"; then
		return 0
	else
		sleep 5
		if denon_ensure_power_status_real "$1" "$2" "$3"; then
			return 0
		fi
	fi
	return 1
}

function denon_ensure_power_on() {
	denon_ensure_power_status 'j' '^k1$' '^k0$'
}

function denon_ensure_power_off() {
	denon_ensure_power_status 'k' '^k0$' '^k1$'
}

function denon_turn_receiver_status() {
	$DENON raw "PW$1" expect "PW$1" > /dev/null
}

function denon_ensure_receiver_status_real() {
	local res=$($DENON raw 'PW?' expect 'PW.*')
	if echo "$res" | grep "^PW$1\$" > /dev/null; then
		return 0
	elif echo "$res" | grep "^PW$2\$" > /dev/null; then
		if denon_turn_receiver_status "$1"; then
			if $DENON raw 'PW?' expect "PW$1" > /dev/null; then
				if [ "x$1" = "xON" ]; then
					sleep 2
				fi
				return 0
			fi
		fi
	fi
	return 1
}

function denon_ensure_receiver_status() {
	if denon_ensure_receiver_status_real "$1" "$2"; then
		return 0
	else
		sleep 5
		if denon_ensure_receiver_status_real "$1" "$2"; then
			return 0
		fi
	fi
	return 1
}

function denon_ensure_receiver_on() {
	denon_ensure_receiver_status 'ON' 'STANDBY'
}

function denon_ensure_receiver_off() {
	denon_ensure_receiver_status 'STANDBY' 'ON'
}

function denon_ensure_masterzone_on() {
	local res=$($DENON raw 'ZM?' expect 'ZM.*')
	if [ $? -ne 0 ]; then
		return $?
	elif check_response "$res" '^ZMON$'; then
		return 0
	elif check_response "$res" '^ZMOFF$'; then
		$DENON raw 'ZMON' expect 'ZMON' > /dev/null
	else
		return 1
	fi
}

function denon_ensure_unmuted() {
	local res=$($DENON mutestatus)
	if [ $? -ne 0 ]; then
		return $?
	elif check_response "$res" '^unmute$'; then
		return 0
	elif check_response "$res" '^mute$'; then
		$DENON unmute
	else
		return 1
	fi
}

function denon_ensure_volume_between() {
	local vol=$($DENON volume)
	if [ $? -ne 0 ]; then
		return $?
	fi
	if (( "$vol" < "$1" )); then
		$DENON volume "$1"
	elif (( "$vol" > "$2" )); then
		$DENON volume "$2"
	fi
}

function denon_ensure_on() {
	local res=$(send_power_command g)
	if check_response "$res" "^k0$"; then
		denon_ensure_power_on || return $?
		sleep 0.4
		local res=$(send_power_command g)
	fi
	if ! check_response "$res" "^k1$"; then
		return 1
	fi
	denon_ensure_receiver_on || return $?
	denon_ensure_masterzone_on || return $?
	denon_ensure_volume_between 100 400 || return $?
	denon_ensure_unmuted || return $?
}

function denon_ensure_off() {
	local res=$(send_power_command g)
	if check_response "$res" "^k0$"; then
		return 0
	fi
	if ! check_response "$res" "^k1$"; then
		return 1
	fi
	denon_ensure_receiver_off || return $?
	sleep 0.4
	denon_ensure_power_off || return $?
}

function denon_ensure_input() {
	local mode=$($DENON raw 'SI?' expect 'SI.*' | sed 's/^SI//')
	if [ $? -ne 0 ]; then
		return $?
	fi
	if [ "x$mode" != "x$1" ]; then
		$DENON raw "SI$1" expect "SI$1" > /dev/null
	fi
}

function denon_ensure_surroundmode() {
	local mode=$($DENON raw 'MS?' expect 'MS.*' | sed 's/^MS//')
	if [ $? -ne 0 ]; then
		return $?
	fi
	if [ "x$mode" != "x$1" ]; then
		$DENON raw "MS$1" expect "MS$1" > /dev/null
	fi
}

function denon_get_state() {
	local res=$(send_power_command 'g')
	if [ $? -ne 0 ]; then
		echo 'error'
		return $?
	fi
	if check_response "$res" '^k0$'; then
		echo 'off'
		return 0
	fi
	if ! check_response "$res" '^k1$'; then
		echo 'error'
		return 1
	fi
	local res=$($DENON raw 'PW?' expect 'PW.*')
	if [ $? -ne 0 ]; then
		echo 'error'
		return $?
	fi
	if check_response "$res" '^PWSTANDBY$'; then
		echo 'halfoff'
		return 0
	fi
	if ! check_response "$res" '^PWON$'; then
		echo 'error'
		return 1
	fi
	local res=$($DENON raw 'ZM?' expect 'ZM.*')
	if [ $? -ne 0 ]; then
		echo 'error'
		return $?
	fi
	if check_response "$res" '^ZMOFF$'; then
		echo 'halfon'
		return 0
	fi
	if ! check_response "$res" '^ZMON$'; then
		echo 'error'
		return 1
	fi
	local res=$($DENON mutestatus)
	if [ $? -ne 0 ]; then
		echo 'error'
		return $?
	fi
	if check_response "$res" '^mute$'; then
		echo 'halfon'
		return 0
	fi
	if ! check_response "$res" '^unmute$'; then
		echo 'error'
		return 1
	fi
	echo 'on'
	return 0
}

function denon_toggle_power() {
	local status=$(denon_get_state)
	if [ $? -ne 0 ]; then
		return $?
	fi
	case "$status" in
	on|halfoff)
		denon_ensure_off
		;;
	off|halfon)
		denon_ensure_on
		;;
	*)
		return 1
		;;
	esac
}

"$@"
exit $?
