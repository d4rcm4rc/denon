/*
    Copyright (C) 2016-2020  Marc André Wittorf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <map>
#include <functional>
#include <memory>
#include <cstdlib>

#include <denon/denon.hpp>

void usage(char*,bool);

class AbstractDenon {
public:
	virtual void sendCommand(const std::string&, const boost::posix_time::time_duration timeout=boost::posix_time::milliseconds(0)) = 0;
	virtual std::string nextCommand(const boost::posix_time::time_duration timeout=boost::posix_time::milliseconds(0)) = 0;
};

template<typename ConcreteDenon>
class PluggableDenon : public AbstractDenon {
public:
	PluggableDenon(ConcreteDenon &denon, denon_compat::boost_io_context &io) : denon(denon), io(io) {}
	PluggableDenon(ConcreteDenon &&denon, denon_compat::boost_io_context &io) : denon(std::move(denon)), io(io) {}
	
	virtual void sendCommand(const std::string &cmd, const boost::posix_time::time_duration timeout=boost::posix_time::milliseconds(0)) { 
		std::shared_ptr<char> status = std::make_shared<char>();
		boost::asio::deadline_timer timer(io);
		if(timeout != boost::posix_time::milliseconds(0)) {
			timer.expires_from_now(timeout);
			timer.async_wait([status=std::weak_ptr<char>(status)](const boost::system::error_code& ec){
				if(auto sstatus = status.lock())
					*sstatus=2;
			});
		}
		denon->sendCommand(cmd, [status=std::weak_ptr<char>(status)](const boost::system::error_code &ec, std::size_t bytes_transferred){
			if(auto sstatus = status.lock())
				*sstatus=ec?3:1;
		});
		if(io.stopped())
			denon_compat::boost_io_context_restart(io);
		do { io.run_one(); } while(*status==0);
		if(*status == 2)
			throw std::runtime_error("timeout");
		else if(*status == 3)
			throw std::runtime_error("error");
	}
	
	virtual std::string nextCommand(const boost::posix_time::time_duration timeout=boost::posix_time::milliseconds(0)) {
		std::shared_ptr<char> status = std::make_shared<char>();
		std::shared_ptr<std::string> res = std::make_shared<std::string>();
		boost::asio::deadline_timer timer(io);
		if(timeout != boost::posix_time::milliseconds(0)) {
			timer.expires_from_now(timeout);
			timer.async_wait([status=std::weak_ptr<char>(status)](const boost::system::error_code& ec){
				if(auto sstatus = status.lock())
					*sstatus=2;
			});
		}
		denon->nextCommand([status=std::weak_ptr<char>(status), res=std::weak_ptr<std::string>(res)](const boost::system::error_code &ec, std::string cmd){
			auto sstatus = status.lock();
			auto sres = res.lock();
			if(sstatus && sres) {
				*sres=cmd;
				*sstatus= ec==boost::asio::error::not_found?4:(ec?3:1);
			}
		});
		if(io.stopped())
			denon_compat::boost_io_context_restart(io);
		do { io.run_one(); } while(*status==0);
		if(*status == 2)
			throw std::runtime_error("timeout");
		else if(*status == 3)
			throw std::runtime_error("error");
		else if(*status == 4)
			throw std::runtime_error("not_found");
		return *res;
	}

private:
	denon_compat::boost_io_context &io;
	ConcreteDenon denon;
};

class DenonController {
public:
	
	DenonController(const std::shared_ptr<AbstractDenon> &denon) : denon(denon) {}
	
	bool on() {
		try {
			sendCommand("PWON");
			_expect(std::regex("PWON"));
			return true;
		} catch (...) {
			return false;
		}
	}
	
	bool off() {
		try {
			sendCommand("PWSTANDBY");
			_expect(std::regex("PWSTANDBY"));
			return true;
		} catch (...) {
			return false;
		}
	}
	
	bool pwstatus() {
		try {
			sendCommand("PW?");
			std::string cmd = _expect(std::regex("(PWSTANDBY)|(PWON)"));
			if(cmd == "PWON")
				std::cout<<"on"<<std::endl;
			else
				std::cout<<"off"<<std::endl;
			return true;
		} catch(...) {
			std::cout<<"error"<<std::endl;
			return false;
		}
	}
	
	bool pwtoggle() {
		try {
			sendCommand("PW?");
			std::string cmd = _expect(std::regex("(PWSTANDBY)|(PWON)"));
			if(cmd == "PWSTANDBY")
				return on();
			else
				return off();
		} catch (...) {
			return false;
		}
	}
	
	bool getmv() {
		try {
			sendCommand("MV?");
			std::string cmd = _expect(regexMV);
			std::smatch match;
			std::regex_match(cmd,match,regexMV);
			std::string oldvol=match[1];
			if(oldvol.length()==2)
				oldvol.append(1,'0');
			int vol = std::stoi(oldvol);
			if(vol==990)
				vol=-5;
			std::cout<<vol<<std::endl;
			return true;
		} catch(...) {
			std::cout<<"error"<<std::endl;
			return false;
		}
	}
	
	bool mvup() {
		try {
			sendCommand("MVUP");
			_expect(regexMV);
			return true;
		} catch (...) {
			return false;
		}
	}
	
	bool mvdown() {
		try {
			sendCommand("MVDOWN");
			_expect(regexMV);
			return true;
		} catch (...) {
			return false;
		}
	}
	
	bool mvud(int n) {
		if(n>1000 || n<-1000)
			return false;
		try {
			sendCommand("MV?");
			std::string cmd = _expect(regexMV);
			std::smatch match;
			std::regex_match(cmd,match,regexMV);
			std::string oldvol=match[1];
			if(oldvol.length()==2)
				oldvol.append(1,'0');
			int vol = std::stoi(oldvol);
			if(vol==990)
				vol=-5;
			vol+=5*n;
			if(vol>800)
				vol=800;
			return mv(vol);
		} catch (...) {
			return false;
		}
	}
	
	bool mv(int vol) {
		try {
			if(vol>800)
				vol=800;
			std::string newvol=std::to_string(vol);
			if(vol<0)
				newvol="99"; //This is bullshit...
			else
				newvol=std::string(3-newvol.length(),'0')+newvol;
			sendCommand("MV"+newvol);
			_expect(regexMV);
			return true;
		} catch (...) {
			return false;
		}
	}
	
	bool mute() {
		try {
			sendCommand("MUON");
			_expect(std::regex("MUON"));
			return true;
		} catch (...) {
			return false;
		}
	}
	
	bool unmute() {
		try {
			sendCommand("MUOFF");
			_expect(std::regex("MUOFF"));
			return true;
		} catch (...) {
			return false;
		}
	}
	
	bool mutestatus() {
		try {
			sendCommand("MU?");
			std::string cmd = _expect(std::regex("(MUON)|(MUOFF)"));
			if(cmd == "MUON")
				std::cout<<"mute"<<std::endl;
			else
				std::cout<<"unmute"<<std::endl;
			return true;
		} catch(...) {
			std::cout<<"error"<<std::endl;
			return false;
		}
	}
	
	bool mutetoggle() {
		try {
			sendCommand("MU?");
			std::string cmd = _expect(std::regex("(MUON)|(MUOFF)"));
			if(cmd == "MUOFF")
				return mute();
			else
				return unmute();
		} catch(...) {
			return false;
		}
	}
	
	bool raw(std::string command) {
		try {
			sendCommand(command);
			return true;
		} catch(...) {
			return false;
		}
	}
	
	bool expect(std::string re) {
		try {
			std::cout<<_expect(std::regex(re))<<std::endl;
			return true;
		} catch(...) {
			return false;
		}
	}
	
	bool watch() {
		try {
			while(true) {
				std::string cmd=denon->nextCommand(mTimeout);
				std::cout<<cmd<<std::endl;
			}
		} catch(std::runtime_error &e) {
			if(strcmp(e.what(),"timeout")) { //daaamn...
				return true;
			} else {
				return false;
			}
		} catch(...) {
			return false;
		}
	}
	
	void setTimeout(boost::posix_time::time_duration timeout) {
		mTimeout=timeout;
	}
	
private:
	void sendCommand(std::string command) {
		denon->sendCommand(command, mTimeout);
	}
	
	std::string _expect(const std::regex &r) {
		boost::posix_time::ptime timeout = boost::posix_time::microsec_clock::local_time()+mTimeout;
		
		while(true) {
			std::string cmd;
			cmd=denon->nextCommand(timeout-boost::posix_time::microsec_clock::local_time());
			std::smatch match;
			if(std::regex_match(cmd,r))
				return cmd;
		}
	}
	
private:
	boost::posix_time::time_duration mTimeout = boost::posix_time::milliseconds(400);
	std::shared_ptr<AbstractDenon> denon;
	
	static const std::regex regexMV;
};
const std::regex DenonController::regexMV("MV([0-9]{2}5?)");


class BaseCommand {
public:
	virtual std::pair<int, std::function<bool(DenonController &)>> operator()(int argc, char *argv[]) = 0;
};

typedef std::function<bool(DenonController&)> CommandClosure;
typedef std::function<std::pair<int,CommandClosure>(int,char*[])> Argparser;

class SimpleCommand : public BaseCommand {
public:
	virtual std::pair<int, std::function<bool(DenonController &)>> operator()(int argc, char *argv[]) override {
		return std::make_pair(1, [fn=this->fn](DenonController &controller) {return fn(&controller);});
	}
	SimpleCommand(std::function<bool(DenonController*)> fn) : fn(fn) {}
	
private:
	std::function<bool(DenonController*)> fn;
};

class VariableCommand : public BaseCommand {
public:
	virtual std::pair<int, std::function<bool(DenonController &)>> operator()(int argc, char *argv[]) override {
		for(auto command : commands) {
			auto res = command(argc,argv);
			if(res.first != 0)
				return res;
		}
		return std::make_pair(0, nullptr);
	}
	VariableCommand(std::vector<Argparser> commands) : commands(std::move(commands)) {}
private:
	std::vector<Argparser> commands;
};

class VolumeSingleCommand : public BaseCommand {
public:
	virtual std::pair<int, std::function<bool (DenonController &)>> operator()(int argc, char *argv[]) override {
		if(argc<2)
			return std::make_pair(0,nullptr);
		if(strcmp(argv[1],"up") == 0)
			return std::make_pair(2,&DenonController::mvup);
		else if(strcmp(argv[1],"down") == 0)
			return std::make_pair(2,&DenonController::mvdown);
		else
			return std::make_pair(0,nullptr);
	}
};

class VolumeMultipleCommand : public BaseCommand {
public:
	virtual std::pair<int, std::function<bool (DenonController &)>> operator()(int argc, char *argv[]) override {
		if(argc<3)
			return std::make_pair(0,nullptr);
		int n;
		if(strcmp(argv[1],"up") == 0)
			n =  1;
		else if(strcmp(argv[1],"down") == 0)
			n = -1;
		else
			return std::make_pair(0,nullptr);
		try {
			n *= std::stoi(argv[2]);
			return std::make_pair(3,[n](DenonController &controller){return controller.mvud(n);});
		} catch(...) {
			return std::make_pair(0,nullptr);
		}
	}
};

class VolumeAbsoluteCommand : public BaseCommand {
public:
	virtual std::pair<int, std::function<bool (DenonController &)>> operator()(int argc, char *argv[]) override {
		if(argc<2)
			return std::make_pair(0,nullptr);
		int n;
		try {
			n = std::stoi(argv[1]);
			return std::make_pair(2,[n](DenonController &controller){return controller.mv(n);});
		} catch(...) {
			return std::make_pair(0,nullptr);
		}
	}
};

class SingleArgCommand : public BaseCommand {
public:
	virtual std::pair<int, std::function<bool (DenonController &)>> operator()(int argc, char *argv[]) override {
		if(argc<2)
			return std::make_pair(0,nullptr);
		try {
			std::string arg = argv[1];
			return std::make_pair(2,[fn=std::bind(fn,std::placeholders::_1,arg)](DenonController &controller){return fn(&controller);});
		} catch(...) {
			return std::make_pair(0,nullptr);
		}
	}
	SingleArgCommand(std::function<bool(DenonController*,std::string)> fn) : fn(fn) {}
	
private:
	std::function<bool(DenonController*,std::string)> fn;
};

class TimeoutCommand : public BaseCommand {
public:
	virtual std::pair<int, std::function<bool (DenonController &)>> operator()(int argc, char *argv[]) override {
		if(argc<2)
			return std::make_pair(0,nullptr);
		try {
			int timeoutms = std::stoi(argv[1]);
			return std::make_pair(2,[timeoutms](DenonController &controller) {
				if(timeoutms>0)
					controller.setTimeout(boost::posix_time::millisec(timeoutms));
				else
					controller.setTimeout(boost::posix_time::pos_infin);
				return true;
			});
		} catch(...) {
			return std::make_pair(0,nullptr);
		}
	}
};

class TCPDescriptor {
public:
	std::shared_ptr<AbstractDenon> operator()(denon_compat::boost_io_context &io) {
		auto sock = std::make_shared<boost::asio::ip::tcp::socket>(io);
		boost::asio::ip::tcp::resolver resolver(io);

#if BOOST_VERSION < 106600
		boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve({host, port});
#else
		boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(host, port).begin();
#endif

		sock->connect(endpoint);
		return std::make_shared<PluggableDenon<typename Denon::TCPDenon::ptr>>(Denon::TCPDenon::make(std::move(sock)), io);
	}
	
	TCPDescriptor(const std::string &spec) {
		std::regex r("(?:(.*):)([0-9]{1,5})");
		std::smatch m;
		if(std::regex_match(spec,m,r)) {
			port = m[2];
			if(m[1] != "")
				host = m[1];
			else
				host = "127.0.0.1";
		} else {
			throw std::invalid_argument("no valid hostname/port pair");
		}
	}
private:
	std::string host;
	std::string port;
};

class UnixDescriptor {
public:
	std::shared_ptr<AbstractDenon> operator()(denon_compat::boost_io_context &io) {
		auto sock = std::make_shared<boost::asio::local::stream_protocol::socket>(io);
		boost::asio::local::stream_protocol::endpoint endpoint(path);
		sock->connect(endpoint);
		return std::make_shared<PluggableDenon<typename Denon::UnixDenon::ptr>>(Denon::UnixDenon::make(std::move(sock)), io);
	}
	
	UnixDescriptor(const std::string &spec) : path(spec) {}
private:
	std::string path;
};

class SerialDescriptor {
public:
	std::shared_ptr<AbstractDenon> operator()(denon_compat::boost_io_context &io) {
		return std::make_shared<PluggableDenon<typename Denon::SerialDenon::ptr>>(Denon::SerialDenon::make(Denon::MakeDenonSerialConnection(io,path)), io);
	}
	
	SerialDescriptor(const std::string &spec) : path(spec) {}
private:
	std::string path;
};


int main(int argc, char *argv[]) {
	/*std::vector<std::string> args(argc-1);
	for(int i = 1; i<argc; ++i)
		args[i-1]=argv[i];*/
	
	//DenonController controller;
	//std::map<std::string, std::function<void()>> commands;
	//commands["volume"]=volume_command;
	//commands["on"]=std::bind(&DenonController::on,&controller);
	//commands["off"]=off_command;
	
	std::map<std::string, Argparser> commands;
	commands["volume"] = VariableCommand({
		VolumeAbsoluteCommand(),
		VolumeMultipleCommand(),
		VolumeSingleCommand(),
		SimpleCommand(&DenonController::getmv)
	});
	commands["pwstatus"] = SimpleCommand(&DenonController::pwstatus);
	commands["on"] = SimpleCommand(&DenonController::on);
	commands["off"] = SimpleCommand(&DenonController::off);
	commands["pwtoggle"] = SimpleCommand(&DenonController::pwtoggle);
	commands["mutestatus"] = SimpleCommand(&DenonController::mutestatus);
	commands["mute"] = SimpleCommand(&DenonController::mute);
	commands["unmute"] = SimpleCommand(&DenonController::unmute);
	commands["mutetoggle"] = SimpleCommand(&DenonController::mutetoggle);
	commands["raw"] = SingleArgCommand(&DenonController::raw);
	commands["expect"] = SingleArgCommand(&DenonController::expect);
	commands["watch"] = SimpleCommand(&DenonController::watch);
	commands["timeout"] = TimeoutCommand();
	
	std::string defEnvDevice = "s:/dev/ttyS0";
	{
		char *envDev = std::getenv("DENON_DEVICE");
		if(envDev)
			defEnvDevice=envDev;
	}
	
	//boost::variant<boost::blank,SerialDescriptor,UnixDescriptor,TCPDescriptor> deviceDescriptor;
	std::function<std::shared_ptr<AbstractDenon>(denon_compat::boost_io_context &)> deviceDescriptor;
	
	std::list<CommandClosure> commandQueue;
	int argidx=1;
	while(argc>argidx) {
		
		if(strcmp(argv[argidx],"-s") == 0 || strcmp(argv[argidx],"--serial") == 0) {
			if(argc-argidx<2)
				usage(argv[0], false);
			deviceDescriptor=SerialDescriptor(argv[argidx+1]);
			argidx+=2;
			continue;
		}
		else if(strcmp(argv[argidx],"-u") == 0 || strcmp(argv[argidx],"--unix") == 0) {
			if(argc-argidx<2)
				usage(argv[0], false);
			deviceDescriptor=UnixDescriptor(argv[argidx+1]);
			argidx+=2;
			continue;
		}
		else if(strcmp(argv[argidx],"-t") == 0 || strcmp(argv[argidx],"--tcp") == 0) {
			if(argc-argidx<2)
				usage(argv[0], false);
			deviceDescriptor=TCPDescriptor(argv[argidx+1]);
			argidx+=2;
			continue;
		}
		else if(commands.count(argv[argidx])) {
			auto res = commands[argv[argidx]](argc-argidx,argv+argidx);
			if(res.first!=0) {
				argidx+=res.first;
				if(res.second != nullptr)
					commandQueue.push_back(std::move(res.second));
				continue;
			}
		}
		
		usage(argv[0], (!strcmp(argv[argidx],"--help") || !strcmp(argv[argidx],"--usage") || !strcmp(argv[argidx],"-h")));
		break;
	}
	
	if(commandQueue.size()==0)
		usage(argv[0],false);
	
	if(!deviceDescriptor) {
		if(defEnvDevice.length()<3 || defEnvDevice[1]!=':') {
			std::cout<<"invalid device specified via DENON_DEVICE"<<std::endl;
			usage(argv[0], false);
		}
		switch(defEnvDevice[0]) {
			case 's':
				deviceDescriptor = SerialDescriptor(defEnvDevice.substr(2));
				break;
			case 'u':
				deviceDescriptor = UnixDescriptor(defEnvDevice.substr(2));
				break;
			case 't':
				deviceDescriptor = TCPDescriptor(defEnvDevice.substr(2));
				break;
			default:
				std::cout<<"invalid device specified via DENON_DEVICE"<<std::endl;
				usage(argv[0], false);
		}
	}
	
	denon_compat::boost_io_context io;
	auto denon = deviceDescriptor(io);
	//denon.sendCommand("MV20");
	DenonController controller(denon);
	//controller.on();
	//controller.mv(0);
	
	bool isFirst=true;
	for(auto command: commandQueue) {
		if(!isFirst)
			usleep(50000);
		else
			isFirst=false;
		if(!command(controller))
			exit(-1);
	}
	
	return 0;
	
	/*int argptr=0;
	while(true) {
		if(commands.count(args[argptr])) {
			commands[args[argptr]]();
			break;
		}
		else {
			usage(argv[0], (args[argptr]=="--help"|| args[argptr]=="--usage" || args[argptr]=="-h"));
			break;
		}
	}*/
}

void usage(char *arg0, bool ok) {
	std::cout
	<<arg0<<": Control a Denon or Marantz AV-Receiver"<<std::endl
	<<"Usage: "<<arg0<<" [options] <command> [arguments] [more commands...]"<<std::endl
	<<"Options:"<<std::endl
	<<"  --help, --usage, -h    Show this help"<<std::endl
	<<"  --serial path, -s path Connect to a serial port"<<std::endl
	<<"  --unix path, -u path   Connect to a unix domain socket"<<std::endl
	<<"  --tcp [host:]port,     Connect to a tcp socket"<<std::endl
	<<"  -t [host:]port"<<std::endl
	<<"Commands:"<<std::endl
	<<"  volume                 Show volume in device-native units"<<std::endl
	<<"  volume <vol>           Set volume to <vol> (must be in device-native format)"<<std::endl
	<<"  volume up [n]          Issue the equivalent of <n> MVUP-commands (command: MVUP or MV? then MVxxx)"<<std::endl
	<<"  volume down [n]        Issue the equivalent of <n> MVDOWN-commands (command: MVDOWN or MV? then MVxxx)"<<std::endl
	<<"  pwstatus               Show if the device is on (command: PW?)"<<std::endl
	<<"  on                     Turn the device on (command: PWON)"<<std::endl
	<<"  off                    Turn the device off (command: PWSTANDBY)"<<std::endl
	<<"  pwtoggle               Turn the device on or off (command: PW?, then PWON or PWOFF)"<<std::endl
	<<"  mutestatus             Mute the device (command: MU?)"<<std::endl
	<<"  mute                   Mute the device (command: MUON)"<<std::endl
	<<"  unmute                 Unmute the device (command: MUOFF)"<<std::endl
	<<"  mutetoggle             Toggle muting (commands: MU?, then MUON or MUOFF)"<<std::endl
	<<"  raw <command>          Send native command to device"<<std::endl
	<<"  watch                  Watch the messages. Timeout is useful here"<<std::endl
	<<"  expect <regex>         Wait for a response matching the regex"<<std::endl
	<<"  timeout <ms>           Set the timeout that will be used from now on"<<std::endl
	<<"Environment variables:"<<std::endl
	<<"  DENON_DEVICE           Can specify device to connect to."<<std::endl
	<<"                         Serial device: s:<path>"<<std::endl
	<<"                         Unix socket:   u:<path>"<<std::endl
	<<"                         TCP socket:    t:[host:]port"<<std::endl;
	exit(!ok);
}
