/*
    Copyright (C) 2020  Marc André Wittorf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DENON_COMPAT_INC
#define DENON_COMPAT_INC

#include <boost/asio.hpp>

namespace denon_compat {
#if BOOST_VERSION < 106600
	typedef boost::asio::io_service boost_io_context;
	static inline void boost_io_context_restart(boost_io_context &io) {
		io.reset();
	}
	template<typename T> static inline void boost_post(boost_io_context &io, T handler) {
		io.post(handler);
	}
	template<typename T> static inline void boost_dispatch(boost::asio::io_service &io, T handler) {
		io.dispatch(handler);
	}
	template<typename C> static inline boost::asio::io_service& boost_get_executor(C &io_container) {
		return io_container.get_io_service();
	}
#else
	typedef boost::asio::io_context boost_io_context;
	static inline void boost_io_context_restart(boost_io_context &io) {
		io.restart();
	}
	template<typename T> static inline void boost_post(boost_io_context &io, T handler) {
		boost::asio::post(io, handler);
	}
	template<typename C, typename T> static inline void boost_dispatch(const C &io, T handler) {
		boost::asio::dispatch(io, handler);
	}
	template<typename C> static inline auto boost_get_executor(C &io_container) {
		return io_container.get_executor();
	}
#endif

}


#endif //DENON_COMPAT_INC
