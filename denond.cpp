/*
    Copyright (C) 2016-2020  Marc André Wittorf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <set>
#include <regex>
#include <cstdio>

#define BOOST_BIND_NO_PLACEHOLDERS
#include <boost/asio.hpp>
#include <boost/signals2.hpp>
#include <boost/program_options.hpp>

#include <sys/types.h>
#include <unistd.h>
#include <grp.h>
#include <pwd.h>

#include <denon/denon.hpp>
#include <denon/asio_helpers.hpp>

template<typename nmspace>
class AbstractServer{
	typedef boost::signals2::signal<void (std::shared_ptr<typename nmspace::socket>)> connected_signal_t;
public:
	typedef typename nmspace::socket socket_type;
	
	AbstractServer(denon_compat::boost_io_context &io) : acceptor(io), nextSocket(io) {}
	
	void listen(void) {
		acceptor.listen();
	}
	
	void async_accept(void) {
		acceptor.async_accept(nextSocket,[this](const boost::system::error_code &ec){
			//std::shared_ptr<TCPConnection> conn = TCPConnection::make(std::move(nextSocket));
			std::shared_ptr<typename nmspace::socket> conn = std::make_shared<typename nmspace::socket>(std::move(nextSocket));
			connected(conn);
			this->async_accept();
		});
	}
	
	auto onConnect(typename connected_signal_t::slot_type &&slot) {
		return connected.connect(std::move(slot));
	}
	
	typename nmspace::acceptor& getAcceptor() {
		return acceptor;
	}
	
protected:
	~AbstractServer(){}
	typename nmspace::acceptor acceptor;
private:
	typename nmspace::socket nextSocket;
	connected_signal_t connected;
};

class TCPServer : public AbstractServer<boost::asio::ip::tcp> {
public:
	using AbstractServer::AbstractServer;
	void bind(std::string address, std::string port) {
#if BOOST_VERSION < 107000
		boost::asio::ip::tcp::resolver resolver(acceptor.get_io_service());
#else
		boost::asio::ip::tcp::resolver resolver(denon_compat::boost_get_executor(acceptor));
#endif
#if BOOST_VERSION < 106600
		boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve({address, port});
#else
		boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(address, port).begin();
#endif
		acceptor.open(endpoint.protocol());
		acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
		acceptor.bind(endpoint);
	}
};

class UnixServer : public AbstractServer<boost::asio::local::stream_protocol>{
public:
	using AbstractServer::AbstractServer;
	void bind(std::string address) {
		std::remove(address.c_str());
		boost::asio::local::stream_protocol::endpoint endpoint (address);
		acceptor.open(endpoint.protocol());
		//acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
		acceptor.bind(endpoint);
	}
};

typedef boost::signals2::signal<void (std::string)> string_signal_t;
typedef AsioHelpers::AsioQueue<std::string> string_queue;

template<typename ServerType>
class DenonServerManager{
	typedef Denon::DenonConnection<std::shared_ptr<typename ServerType::socket_type>> MyDenonConnection;
public:
	template<typename... Args>
	DenonServerManager(denon_compat::boost_io_context &io, Args&&... args)
	: io(io)
	, server(std::make_unique<ServerType>(std::forward<Args>(args)...))
	{}
	
	//DenonServerManager() = default;
	DenonServerManager(DenonServerManager<ServerType> &&) = default;
	//DenonServerManager<ServerType>& operator=(DenonServerManager<ServerType>&&) = default;
	
	template<typename... Args>
	void bind(Args&&... args) {
		server->bind(std::forward<Args>(args)...);
	}
	
	void setup(string_signal_t &inputSignal, string_queue &outputQueue) {
		auto &io = this->io;
		server->onConnect([&io,&inputSignal,&outputQueue](std::shared_ptr<typename ServerType::socket_type> conn){
			typename MyDenonConnection::ptr dconn = MyDenonConnection::make(std::move(conn));
			//Denon -> Client
			auto strand = std::make_shared<denon_compat::boost_io_context::strand>(io);
			struct{
				void operator()(std::string response) {
#if BOOST_VERSION < 106600
					this->strand->dispatch([dconn=this->dconn, answerConn=this->answerConn, response = std::move(response)](){
#else
					boost::asio::dispatch(this->strand->context(), [dconn=this->dconn, answerConn=this->answerConn, response = std::move(response)](){
#endif
						dconn->sendCommand(response,[dconn=std::move(dconn),answerConn=std::move(answerConn)](const boost::system::error_code& ec, std::size_t bytes_transferred){
							if(ec) {
								dconn->close();
								answerConn.disconnect();
							}
						});
					});
				}
				typename MyDenonConnection::ptr dconn;
				std::shared_ptr<denon_compat::boost_io_context::strand> strand;
				boost::signals2::connection answerConn;
			} answerFunc = {.dconn=dconn, .strand=strand};
			boost::signals2::connection answerConn = inputSignal.connect(answerFunc);
			answerFunc.answerConn=answerConn;
			//Client -> Denon
			pump(std::move(dconn),outputQueue,std::move(answerConn));
		});
	}
	
	ServerType& getServer() {
		return *server;
	}
	
	void start(void) {
		server->listen();
		server->async_accept();
	}

private:
	static void pump(typename MyDenonConnection::ptr &&dconn, string_queue &outputQueue, boost::signals2::connection &&answerConn) {
		typename MyDenonConnection::ptr::element_type *dcptr=dconn.get();
		dcptr->nextCommand([dconn=std::move(dconn), &outputQueue, answerConn=std::move(answerConn)](const boost::system::error_code &ec, std::string cmd) mutable {
			if(ec && ec != boost::asio::error::not_found) {
				dconn->close();
				answerConn.disconnect();
				return;
			}
			if(!ec)
				outputQueue.push(std::move(cmd));
			pump(std::move(dconn), outputQueue, std::move(answerConn));
		});
	}
	
	denon_compat::boost_io_context &io;
	std::unique_ptr<ServerType> server;
};
typedef DenonServerManager<TCPServer> TCPDenonServerManager;
typedef DenonServerManager<UnixServer> UnixDenonServerManager;

// Other Stuff
//////////////
uid_t resolveUser(std::string user) {
	uid_t uid;
	struct passwd *upwd;
	
	upwd = getpwnam(user.c_str());
	if(!upwd) {
		throw std::invalid_argument("User not found");
	}
	uid = upwd->pw_uid;
	return uid;
}
gid_t resolveGroup(std::string group) {
	gid_t gid;
	struct group  *gpwd;
	gpwd = getgrnam(group.c_str());
	if(!gpwd) {
		throw std::invalid_argument("Group not found");
	}
	gid=gpwd->gr_gid;
	return gid;
}

void dropPrivileges(std::string user, std::string group) {
	uid_t uid=getuid();
	gid_t gid=getgid();
	if(geteuid()==0) {
		if(setgroups(0,NULL) == -1) {
			std::cerr<<"Could not drop groups."<<std::endl;
		}
		uid=resolveUser(user);
		gid=resolveGroup(group);
	}
	if (setgid(gid) == -1) {
		std::cerr<<"Could not drop gid."<<std::endl;
		exit(1);
	}
	if (setuid(uid) == -1) {
		std::cerr<<"Could not drop uid."<<std::endl;
		exit(1);
	}
}

//main stuff
////////////

struct TcpDescriptor{
	TcpDescriptor(std::string port, std::string address="127.0.0.1"): port(port),address(address) {}
	std::string port,address;
};
void validate(boost::any& v,
              const std::vector<std::string>& values,
              TcpDescriptor* target_type, int) {
	static std::regex r("(?:(.*):)?([0-9]{1,5})");
	boost::program_options::validators::check_first_occurrence(v);
	const std::string& s = boost::program_options::validators::get_single_string(values);
	std::smatch match;
	if(std::regex_match(s,match,r)) {
		if(match[1]=="")
			v = boost::any(TcpDescriptor(match[2]));
		else
			v = boost::any(TcpDescriptor(match[2],match[1]));
	} else {
		throw boost::program_options::validation_error(boost::program_options::validation_error::invalid_option_value);
	}
}
struct UnixDescriptor{
	UnixDescriptor(std::string path, mode_t mode=660, std::string user="", std::string group=""): path(path), mode(mode){
		if(user == "")
			uid=getuid();
		else
			uid=resolveUser(user);
		if(group == "")
			gid=getgid();
		else
			gid=resolveGroup(group);
	}
	std::string path;
	mode_t mode;
	uid_t uid;
	gid_t gid;
};
void validate(boost::any& v,
              const std::vector<std::string>& values,
              UnixDescriptor* target_type, int) {
	//I don't really like this...
	static std::regex r("(?:(?:(?:(.*):)?(.*):)?([0-7]{3}):)?(.*)");
	boost::program_options::validators::check_first_occurrence(v);
	const std::string& s = boost::program_options::validators::get_single_string(values);
	std::smatch match;
	if(std::regex_match(s,match,r)) {
		if(match[3]=="")
			v = boost::any(UnixDescriptor(match[4]));
		else
			v = boost::any(UnixDescriptor(match[4], std::stoi(match[3]), match[1], match[2]));
	} else {
		throw boost::program_options::validation_error(boost::program_options::validation_error::invalid_option_value);
	}
}

void pumpDenonToClient(string_signal_t &answerSignal, Denon::SerialDenon::ptr &denon) {
	denon->nextCommand([&answerSignal,&denon](const boost::system::error_code &ec, std::string cmd) {
		if(ec) {
		} else {
			answerSignal(cmd);
			pumpDenonToClient(answerSignal,denon);
		}
	});
}

void pumpClientToDenon(string_queue &requestQueue, Denon::SerialDenon::ptr &denon) {
	requestQueue.pop([&requestQueue, &denon](std::string cmd) {
		denon->sendCommand(cmd,[&requestQueue, &denon](const boost::system::error_code &ec, std::size_t bytes_transferred) {
			pumpClientToDenon(requestQueue, denon);
		});
	});
}

int main(int argc, char *argv[]) {
	denon_compat::boost_io_context io;
	
	//=========================================================================
	//Option parsing
	//=========================================================================
	boost::program_options::options_description desc("Allowed Options");
	desc.add_options()
		("help,h",                                                                              "Show this help")
		("user,u",   boost::program_options::value<std::string>()->default_value("nobody"),     "After setting up, continue execution as user (only if effective user is root)")
		("group,g",  boost::program_options::value<std::string>()->default_value("nobody"),     "After setting up, continue execution as group (only if effective user is root)")
		("tcp,t",    boost::program_options::value<std::vector<TcpDescriptor>>(),               "Listen on this tcp port. Format: [address:]port. Can be supplied multiple times.")
		("unix,s",   boost::program_options::value<std::vector<UnixDescriptor>>(),              "Listen on this unix socket. Format: [[user[:group]:]mode:]path. Can be supplied multiple times. Use user::mode:path to set user but not group.")
		("device,d", boost::program_options::value<std::string>()->default_value("/dev/ttyS0"), "Use this (serial) device.");
	
	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc),vm);
	boost::program_options::notify(vm);
	
	if(vm.count("help")) {
		desc.print(std::cout);
		return 0;
	}
	std::string device = vm["device"].as<std::string>();
	std::vector<TcpDescriptor> tcpDescriptors(vm.count("tcp")?vm["tcp"].as<std::vector<TcpDescriptor>>():std::vector<TcpDescriptor>());
	std::vector<UnixDescriptor> unixDescriptors(vm.count("unix")?vm["unix"].as<std::vector<UnixDescriptor>>():std::vector<UnixDescriptor>());
	std::string user = vm["user"].as<std::string>();
	std::string group = vm["group"].as<std::string>();
	
	//=========================================================================
	//Interface between Serial and Network stuff
	//=========================================================================
	string_signal_t answerSignal;
	string_queue requestQueue(io);
	
	//=========================================================================
	//Serial Stuff
	//=========================================================================
	Denon::SerialDenon::ptr denon(Denon::SerialDenon::make(Denon::MakeDenonSerialConnection(io, device)));
	//Denon -> Client
	pumpDenonToClient(answerSignal,denon);
	//Client -> Denon
	pumpClientToDenon(requestQueue,denon);
	
	
	//=========================================================================
	//Network Stuff
	//=========================================================================
	
	std::vector<TCPDenonServerManager> tcpServers;
	std::vector<UnixDenonServerManager> unixServers;
	
	for(auto desc : tcpDescriptors) {
		TCPDenonServerManager tcpServer(io, io);
		tcpServer.bind(desc.address,desc.port);
		tcpServer.setup(answerSignal,requestQueue);
		tcpServers.push_back(std::move(tcpServer));
	}
	
	for(auto desc : unixDescriptors) {
		UnixDenonServerManager unixServer(io, io);
		unixServer.bind(desc.path);
		unixServer.setup(answerSignal,requestQueue);
		boost::asio::local::stream_protocol::acceptor& acceptor=unixServer.getServer().getAcceptor();
		int acceptor_fd = acceptor.native_handle();
		if(chown(desc.path.c_str(),desc.uid,desc.gid) == -1) {
			std::cout<<"Could not chown the socket "<<std::endl;
			exit(1);
		}
		if(chmod(desc.path.c_str(), desc.mode) == -1) {
			std::cout<<"Could not chmod the socket "<<std::endl;
			exit(1);
		}
		unixServers.push_back(std::move(unixServer));
	}
	
	for(auto &server : tcpServers)
		server.start();
	for(auto &server : unixServers)
		server.start();
	
	dropPrivileges(user,group);
	
	io.run();
	
	return 0;
}

/* System Functions
setuid
getuid
setgid
getgid
setgroups
getgroups
getpwnam
chown
chmod
 */
